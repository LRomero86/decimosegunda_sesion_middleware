const compression = require('compression');
const express = require('express');
const server = express();


server.use(compression());

//Configurar Middlewares

server.use(function(req, res, next){
    console.log(req.url);
    next();
});

//middleware global

server.use(function(req, res, next){
    console.log('Time: ', Date.now());
})

//middleware de aplicación

function validarUsuario(req, res, next){
    if(req.query.usuario != 'admin'){
        res.json('Usuario inválido');
    }else{
        res.json('Usuario válido');
        next();
    }
}

//endpoints

server.get('/', function(req, res){
    console.log('Hola mundo desde el get');
    res.json('Hola mundo!');
})

server.get('/saludo_query/usuario', validarUsuario, function(req, res){

    let data = req.query;
    res.json('Usuario inválido');

});

server.use('/demo', interceptar, function(req, res, next){
    res.json('Hola Mundo, interceptor');
    next();
})

server.get('/probarcompression', function(req, res){
    const animal = 'Alligator';
    //send a text/html fileback with the word 'Alligator' repeated 10000 times
    res.send(animal.repeat(1000));
})





