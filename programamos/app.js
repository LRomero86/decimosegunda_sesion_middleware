const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json);

const users = [
    {id: 1, nombre: "pepe", email= "pepe@mail.com"},
    {id: 2, nombre: "jose", email= "jose@mail.com"},
    {id: 3, nombre: "nadia", email= "nadia@mail.com"},
]


app.get('/users', function(req, res){
    res.json(users);
});






app.listen(3000, function () { 
    console.log("corriendo en puerto 3000")   
});