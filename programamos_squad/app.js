const express = require('express');
const app = express();

const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Acamica API',
            version: '1.0.0',
      }
    },
    apis: ['./app.js'],
  };
  
const swaggerDocs = swaggerJsDoc(options);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));


const autores = [
    {
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        fechaDeNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus más conocidas obras...",
                anioPub: 1944,
            },
            {
                id: 2,
                titulo: "El Aleph",
                descripcion: "Otra recopilación de cuentos...",
                anioPub: 1949,
            }

        ]
    },
    {
        id: 2,
        nombre: "Julio",
        apellido: "Cortázar",
        fechaDeNacimiento: "26/08/1914",
        libros: [
            {
                id: 1,
                titulo: "Historias de cronopios y de famas",
                descripcion: "Nostrud occaecat cupidatat excepteur enim esse cillum laborum ea voluptate ex et eiusmod.",
                anioPub: 1962
            },
            {
                id: 2,
                titulo: "La vuelta al día en ochenta mundos",
                descripcion: "Enim quis ut laborum eiusmod laborum id deserunt pariatur tempor.",
                anioPub: 1967,
            }
        ]
    }
]

//GET: devuelve autores


app.get('/autores', function(req, res){
    res.send(autores);
})



app.listen(3000, function () { 
    console.log("corriendo en puerto 3000")   
});

/**
 * @swagger
 * /autores:
 *  libros:
 *
 */
 app.post('/autores', (req, res) => {
    res.status(201).send();
  });